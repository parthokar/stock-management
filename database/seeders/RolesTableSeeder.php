<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            // Seed roles
            Role::create(['name' => 'admin']);
            Role::create(['name' => 'customer']);
            Role::create(['name' => 'manager']);
            // Add more roles as needed

    }
}
