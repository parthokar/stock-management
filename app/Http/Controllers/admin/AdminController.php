<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;



class AdminController extends Controller
{

    //admin dashboard
    public function index()
    {

        return view('dashboard.admin.admin-dashboard');
    }
}
