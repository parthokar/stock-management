@extends('layouts.app')
@section('title')
    Create User
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Import User</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <h4 class="mt-3 mb-3">Import User</h4>
                            @if (count($errorMessages) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errorMessages as $errorMessage)
                                        <li>{{ $errorMessage }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif



                           <a href="{{asset('/user/sample_user.csv')}}">Download Sample CSV</a>
                            <form action="{{ route('import.users') }}" method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="row mb-3">
                                    <label for="name"
                                        class="col-md-4 col-form-label text-md-end">{{ __('Upload File') }}
                                        <span style="color:red">*</span></label>
                                    <div class="col-md-6">
                                        <input id="name" type="file" accept=".xlsx, .xls, .csv"
                                            class="form-control @error('file') is-invalid @enderror" name="file" required
                                            autocomplete="name" autofocus>

                                    </div>
                                </div>


                                <div class="row mb-3 mt-3">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Import') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
