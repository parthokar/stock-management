<?php

function sidebarMenuChange($routeName) {
    // Get the authenticated user
    $user = auth()->user();

    // Check if the user has the 'admin' role (adjust this condition according to your role logic)
    if ($user->role_id === 1) {
        return true; // Show the menu for admin role
    }

    // Get the role's permissions
    $permissions = \DB::table('role_permissions')->where('role_id', $user->role_id)->pluck('permission_url')->toArray();

    // Check if the requested route name is in the list of permissions
    if (in_array($routeName, $permissions)) {
        return true; // Show the menu for users with permission
    }

    return false; // Hide the menu for users without permission
}


