<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        // Seed permissions
        Permission::create(['name' => 'User List','url'=>'manage-user.index', 'description' => 'Create new user']);
        Permission::create(['name' => 'User add','url'=>'manage-user.create', 'description' => 'Create new user']);
        Permission::create(['name' => 'User edit','url'=>'manage-user.edit', 'description' => 'Edit existing user']);
        Permission::create(['name' => 'User store','url'=>'manage-user.store', 'description' => 'Edit existing user']);
        Permission::create(['name' => 'User update','url'=>'manage-user.update', 'description' => 'Edit existing user']);
        Permission::create(['name' => 'User import','url'=>'import_user', 'description' => 'Edit existing user']);
        Permission::create(['name' => 'User Csv Upload','url'=>'import.users', 'description' => 'Edit existing user']);

        Permission::create(['name' => 'Role List','url'=>'roles.index', 'description' => 'Delete user']);
        Permission::create(['name' => 'Role add','url'=>'roles.create', 'description' => 'Delete user']);
        Permission::create(['name' => 'Role edit','url'=>'roles.edit', 'description' => 'Delete user']);
        Permission::create(['name' => 'Role store','url'=>'roles.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Role update','url'=>'roles.update', 'description' => 'Delete user']);

        Permission::create(['name' => 'Category List','url'=>'course-category.index', 'description' => 'Create new user']);
        Permission::create(['name' => 'Category add', 'url'=>'course-category.create','description' => 'Create new user']);
        Permission::create(['name' => 'Category edit','url'=>'course-category.edit', 'description' => 'Edit existing user']);
        Permission::create(['name' => 'Category store','url'=>'course-category.store', 'description' => 'Edit existing user']);
        Permission::create(['name' => 'Category update','url'=>'course-category.update', 'description' => 'Edit existing user']);

        Permission::create(['name' => 'Course List','url'=>'course.index', 'description' => 'Delete user']);
        Permission::create(['name' => 'Course add', 'url'=>'course.create','description' => 'Delete user']);
        Permission::create(['name' => 'Course edit','url'=>'course.edit', 'description' => 'Delete user']);
        Permission::create(['name' => 'Course store','url'=>'course.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Course update','url'=>'course.update', 'description' => 'Delete user']);
        Permission::create(['name' => 'Course enroll List', 'url'=>'course-enroll.index','description' => 'Delete user']);
        Permission::create(['name' => 'Course enroll Edit', 'url'=>'course-enroll.edit','description' => 'Delete user']);
        Permission::create(['name' => 'Course enroll Update', 'url'=>'course-enroll.update','description' => 'Delete user']);
        Permission::create(['name' => 'Student Course Enroll Store', 'url'=>'student-course-enroll.store','description' => 'Delete user']);

        Permission::create(['name' => 'student payment List','url'=>'student-payment.index', 'description' => 'Create new user']);
        Permission::create(['name' => 'student payment add','url'=>'student-payment.create', 'description' => 'Create new user']);
        Permission::create(['name' => 'student payment store','url'=>'student-payment.store', 'description' => 'Create new user']);
        Permission::create(['name' => 'student payment details','url'=>'pay_details', 'description' => 'Edit existing user']);

        Permission::create(['name' => 'Exam List', 'url'=>'exam.index','description' => 'Delete user']);
        Permission::create(['name' => 'Exam add','url'=>'exam.create', 'description' => 'Delete user']);
        Permission::create(['name' => 'Exam edit','url'=>'exam.edit', 'description' => 'Delete user']);
        Permission::create(['name' => 'Exam store','url'=>'exam.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Exam update','url'=>'exam.update', 'description' => 'Delete user']);
        Permission::create(['name' => 'Exam result create','url'=>'get_certificate', 'description' => 'Delete user']);

        Permission::create(['name' => 'Quiz List','url'=>'quiz.index', 'description' => 'Delete user']);
        Permission::create(['name' => 'Quiz add','url'=>'quiz.create', 'description' => 'Delete user']);
        Permission::create(['name' => 'Quiz edit','url'=>'quiz.edit', 'description' => 'Delete user']);
        Permission::create(['name' => 'Quiz store','url'=>'quiz.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Quiz update','url'=>'quiz.update', 'description' => 'Delete user']);
        Permission::create(['name' => 'Quiz result create','url'=>'get_quiz', 'description' => 'Delete user']);

        Permission::create(['name' => 'Announcement List','url'=>'announcement.index', 'description' => 'Delete user']);
        Permission::create(['name' => 'Announcement add','url'=>'announcement.create', 'description' => 'Delete user']);
        Permission::create(['name' => 'Announcement edit','url'=>'announcement.edit', 'description' => 'Delete user']);
        Permission::create(['name' => 'Announcement store','url'=>'announcement.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Announcement update','url'=>'announcement.update', 'description' => 'Delete user']);

        Permission::create(['name' => 'Promocode List','url'=>'promo_index', 'description' => 'Delete user']);
        Permission::create(['name' => 'Promocode add','url'=>'promo_create', 'description' => 'Delete user']);
        Permission::create(['name' => 'Promocode edit','url'=>'promo_edit', 'description' => 'Delete user']);
        Permission::create(['name' => 'Promocode store','url'=>'promo_store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Promocode update','url'=>'promo_update', 'description' => 'Delete user']);

        Permission::create(['name' => 'Send email','url'=>'send-email.index', 'description' => 'Delete user']);
        Permission::create(['name' => 'Send email user','url'=>'send-email.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Send sms','url'=>'send-sms.index', 'description' => 'Delete user']);
        Permission::create(['name' => 'Channel List','url'=>'channel_route', 'description' => 'Delete user']);
        Permission::create(['name' => 'Channel add','url'=>'channel_route_create', 'description' => 'Delete user']);
        Permission::create(['name' => 'Channel store','url'=>'channel_route_store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Activity log','url'=>'activity-log.index', 'description' => 'Delete user']);

        Permission::create(['name' => 'Settings List','url'=>'system-settings.index', 'description' => 'Delete user']);
        Permission::create(['name' => 'Settings create','url'=>'system-settings.create', 'description' => 'Delete user']);
        Permission::create(['name' => 'Settings store','url'=>'system-settings.store', 'description' => 'Delete user']);


        Permission::create(['name' => 'permission list','url'=>'permission_list', 'description' => 'Delete user']);
        Permission::create(['name' => 'permission edit','url'=>'permission_edit', 'description' => 'Delete user']);
        Permission::create(['name' => 'permission update','url'=>'permission_update', 'description' => 'Delete user']);
        Permission::create(['name' => 'Save Exam Question','url'=>'question-exam.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Update Exam Question','url'=>'question-exam.update', 'description' => 'Delete user']);
        Permission::create(['name' => 'Update Quiz Question','url'=>'question-quiz.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Update Quiz Question','url'=>'question-quiz.update', 'description' => 'Delete user']);




        Permission::create(['name' => 'Video list','url'=>'video.index', 'description' => 'Delete user']);
        Permission::create(['name' => 'Video add','url'=>'video.create', 'description' => 'Delete user']);
        Permission::create(['name' => 'Video store','url'=>'video.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'Video edit','url'=>'video.edit', 'description' => 'Delete user']);
        Permission::create(['name' => 'Video update','url'=>'video.update', 'description' => 'Delete user']);

        Permission::create(['name' => 'company list','url'=>'company.index', 'description' => 'Delete user']);
        Permission::create(['name' => 'company create','url'=>'company.create', 'description' => 'Delete user']);
        Permission::create(['name' => 'company store','url'=>'company.store', 'description' => 'Delete user']);
        Permission::create(['name' => 'company edit','url'=>'company.edit', 'description' => 'Delete user']);
        Permission::create(['name' => 'company update','url'=>'company.update', 'description' => 'Delete user']);
        Permission::create(['name' => 'contact List','url'=>'contact-student.index', 'description' => 'Delete user']);


        Permission::create(['name' => 'Student Exam','url'=>'student-exam.show', 'description' => 'Delete user']);
        Permission::create(['name' => 'Student Exam Submit','url'=>'student-exam.store', 'description' => 'Delete user']);

        Permission::create(['name' => 'Student Quiz','url'=>'student-quiz.show', 'description' => 'Delete user']);
        Permission::create(['name' => 'Student Quiz Submit','url'=>'student-quiz.store', 'description' => 'Delete user']);
        // Add more permissions as needed
    }
}

