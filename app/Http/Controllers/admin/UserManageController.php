<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use DataTables;
use App\Jobs\ResetPassword;
use Illuminate\Support\Str;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;



class UserManageController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = User::orderBy('users.id', 'desc')
                ->leftjoin('roles', 'roles.id', '=', 'users.role_id')
                ->select('users.*', 'roles.name as role')
                ->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return 'Active';
                    } else {
                        return 'Inactive';
                    }
                })
                ->addColumn('user_id', function ($row) {

                    return '#00' . $row->id;

                })
                ->addColumn('role', function ($row) {

                    return $row->role;

                })
                ->addColumn('action', function ($row) {
                    return '<a href="' . route('manage-user.edit', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>';
                })
                ->rawColumns(['status', 'user_id', 'role', 'action'])
                ->make(true);
        }
        return view('dashboard.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::latest()->get();
        return view('dashboard.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'max:255', 'unique:users'],
             'password' => ['required', 'string', 'min:8', 'regex:/^(?=.*[A-Za-z])(?=.*\d).+$/', 'confirmed'],
            'role_id' => ['required'],
        ]);

      $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'role_id' => $request->role_id,
            'password' => Hash::make($request->password),
            'status' => $request->status,
        ]);

           // Generate a reset token
           $token = Str::random(64);
           DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
          ]);


        ResetPassword::dispatch($request->email,$token);

        return redirect()->route('manage-user.index')->with('success', 'user has been created successfully.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CourseCategory  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::find($id);
        $roles = Role::latest()->get();
        return view('dashboard.users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);
        if ($request->password != null) {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => 'required|unique:users,email,' . $user->id,
                'phone' => 'required|unique:users,phone,' . $user->id,
                'password' => ['required', 'string', 'confirmed'],
                'role_id' => ['required'],
            ]);
        } else {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => 'required|unique:users,email,' . $user->id,
                'phone' => 'required|unique:users,phone,' . $user->id,
                'role_id' => ['required'],
            ]);
        }


        User::find($id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => $request->password ? Hash::make($request->password) : $user->password,
            'role_id' => $request->role_id,
            'status' => $request->status,
        ]);


        return redirect()->route('manage-user.index')->with('success', 'User has been updated successfully');
    }

}
