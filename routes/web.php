<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\admin\SupplierController;
use App\Http\Controllers\admin\CustomerController;
use App\Http\Controllers\admin\CategoryController;
use App\Http\Controllers\admin\BrandController;
use App\Http\Controllers\admin\ProductController;
use App\Http\Controllers\admin\UserManageController;






/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', [HomeController::class, 'index'])->name('home_route');

    Route::get('/admin-dashboard', [AdminController::class, 'index'])->name('admin.dashboard');

        Route::resource('manage-user', UserManageController::class);

        Route::resource('supplier', SupplierController::class);

        Route::resource('customer', CustomerController::class);

        Route::resource('category', CategoryController::class);

        Route::resource('brand', BrandController::class);

        Route::resource('product', ProductController::class);

    });


