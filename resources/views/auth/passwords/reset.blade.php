@extends('layouts.app_front')
@section('title')  @endsection
@section('content')

<div id="about-us" class="section mt-100">
    <div id="registration" class="section">
        <div class="container w-container">
            <div class="flex-space-between">
                <div data-w-id="3a25ada7-037e-f3e3-739a-77b04b494cc7" style="opacity: 1; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg); transform-style: preserve-3d;" class="full_width text-center flex-center mb-20">
                    <h2 class="heading-h2 cc-section-title text-nv">
                        Reset Password</h2><div class="divider-full cc-small cc-section-title bg-nv">
                            </div>
                        </div>
                        <div class="full_width">
                            <div class="w-form">
                                <form method="POST" action="{{ route('password.update') }}">
                                    @csrf
                                    <input type="hidden" name="token" value="{{ $token }}">
                                    <div class="flex-space-between cc-small">
                                        <div class="flex-width-12">
                                            <label for="student_email">Email</label>
                                            <input type="email" class="w-input" maxlength="256" name="email" value="{{ old('email') }}" data-name="student_email" placeholder="Enter your valid email here" id="student_email" required="">
                                            @error('email')
                                                <strong>{{ $message }}</strong>
                                            @enderror
                                        </div>

                                        <div class="flex-width-12"><label for="student_current_job">Password</label>
                                            <input type="password" class="w-input" maxlength="256" name="password" data-name="student_current_job" placeholder="Enter password" id="student_current_job" required="">
                                            <span>Password pattern must be alpha numeric format minimum 8 charecter</span>
                                            <br/>
                                            @error('password')
                                                    <strong>{{ $message }}</strong>
                                            @enderror
                                        </div>

                                        <div class="flex-width-12"><label for="student_current_job">Confirm Password</label>
                                            <input type="password" class="w-input" maxlength="256" name="password_confirmation" data-name="student_current_job" placeholder="Enter password" id="student_current_job" required="">
                                            <span>Password pattern must be alpha numeric format minimum 8 charecter</span>
                                        </div>


                                    </div>
                                    <input type="submit" value="Reset Password" data-wait="Please wait..." class="button-df mt-20 w-button">
                                </form>
                                <div class="w-form-done" tabindex="-1" role="region" aria-label="Email Form success">
                                    <div>
                                        Thank you! Your submission has been received!
                                    </div>
                                </div>
                                <div class="w-form-fail" tabindex="-1" role="region" aria-label="Email Form failure">
                                    <div>Oops! Something went wrong while submitting the form.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</div>
@endsection
