@extends('layouts.app')
@section('title')
    Create Product
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0"></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Create Product</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <h4 class="mt-3 mb-3">Create Product</h4>
                            <form method="POST" action="{{ route('product.store') }}" enctype="multipart/form-data">
                                @csrf

                                <div class="row mb-3">
                                    <label for="name"
                                        class="col-md-4 col-form-label text-md-end">{{ __('Product Name') }} <span
                                            style="color:red">*</span></label>

                                    <div class="col-md-6">
                                        <input id="name" type="text"
                                            class="form-control @error('product_name') is-invalid @enderror"
                                            name="product_name" value="{{ old('product_name') }}" required
                                            autocomplete="name" autofocus>

                                        @error('product_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="password-confirm"
                                        class="col-md-4 col-form-label text-md-end">{{ __('Select Category') }} <span
                                            style="color:red">*</span></label>

                                    <div class="col-md-6">
                                        <select class="form-control" name="category_id" required>
                                            <option value="">Select Category </option>
                                            @foreach ($data['category'] as $categorys)
                                                <option value="{{ $categorys->id }}">{{ $categorys->category_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('category_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="password-confirm"
                                        class="col-md-4 col-form-label text-md-end">{{ __('Select Brand') }} <span
                                            style="color:red">*</span></label>

                                    <div class="col-md-6">
                                        <select class="form-control" name="brand_id" required>
                                            <option value="">Select Brand </option>
                                            @foreach ($data['brand'] as $brands)
                                                <option value="{{ $brands->id }}">{{ $brands->brand_name }}</option>
                                            @endforeach
                                        </select>
                                        @error('brand_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="name"
                                        class="col-md-4 col-form-label text-md-end">{{ __('Product Price') }} <span
                                            style="color:red">*</span></label>

                                    <div class="col-md-6">
                                        <input id="name" type="number"
                                            class="form-control @error('price') is-invalid @enderror" name="price"
                                            value="{{ old('price') }}" required autocomplete="name" autofocus>

                                        @error('price')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="name"
                                        class="col-md-4 col-form-label text-md-end">{{ __('Stock Alert Quantity') }} <span
                                            style="color:red">*</span></label>

                                    <div class="col-md-6">
                                        <input id="name" type="number"
                                            class="form-control @error('stock_alert_qty') is-invalid @enderror"
                                            name="stock_alert_qty" value="{{ old('stock_alert_qty') }}" required
                                            autocomplete="name" autofocus>

                                        @error('stock_alert_qty')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="name"
                                        class="col-md-4 col-form-label text-md-end">{{ __('Product Image') }} <span
                                            style="color:red">*</span></label>

                                    <div class="col-md-6">
                                        <input id="name" type="file"
                                            class="form-control @error('image') is-invalid @enderror" name="image"
                                            required autocomplete="name" autofocus>

                                        @error('image')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>



                                <div class="row mb-3 mt-3">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Submit') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
