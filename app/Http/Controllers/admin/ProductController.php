<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\Product;

use App\Models\Category;

use App\Models\Brand;

use DataTables;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) {

            $data = Product::with('category', 'brand')->orderBy('id', 'desc')->get();

            return Datatables::of($data)

                ->addIndexColumn()

                ->addColumn('image', function ($row) {
                    return '<img src="' . asset('dashboard/product/' . $row->image) . '" width="50%" height="50%" class="img-fluid">';
                })

                ->addColumn('categorys', function ($row) {
                    return optional($row->category)->category_name;
                })

                ->addColumn('brands', function ($row) {
                    return optional($row->brand)->brand_name;
                })

                ->addColumn('action', function ($row) {
                    return '<a href="' . route('product.edit', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>';
                })

                ->rawColumns(['image', 'categorys', 'brands', 'action'])

                ->make(true);
        }
        return view('dashboard.product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['category'] = Category::get();

        $data['brand'] = Brand::get();

        return view('dashboard.product.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'category_id' => ['required'],
            'brand_id' => ['required'],
            'product_name' => ['required'],
            'price' => ['required'],
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);

        // Get the file from the request
        $image = $request->file('image');

        // Generate a unique name for the file to avoid conflicts
        $filename = time() . '.' . $image->getClientOriginalExtension();

        // Save the image to the public folder
        $image->move('dashboard/product/', $filename);


        Product::create([
            'category_id' => $request->category_id,
            'brand_id' => $request->brand_id,
            'product_name' => $request->product_name,
            'price' => $request->price,
            'image' => $filename,
            'stock_alert_qty' => $request->stock_alert_qty,
        ]);

        return redirect()->route('product.index')->with('success', 'Product has been created successfully.');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['product'] = Product::find($id);

        $data['category'] = Category::get();

        $data['brand'] = Brand::get();

        return view('dashboard.Product.edit', compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $product = Product::find($id);

        $request->validate([
            'category_id' => ['required'],
            'brand_id' => ['required'],
            'product_name' => ['required'],
            'price' => ['required'],
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
        ]);

        if ($request->hasFile('image')) {
            // Get the file from the request
            $image = $request->file('image');

            // Generate a unique name for the file to avoid conflicts
            $filename = time() . '.' . $image->getClientOriginalExtension();

            // Save the image to the public folder
            $image->move('dashboard/product/', $filename);
        } else {
            $filename = $product->image;
        }


        Product::find($id)->update([
            'category_id' => $request->category_id,
            'brand_id' => $request->brand_id,
            'product_name' => $request->product_name,
            'price' => $request->price,
            'image' => $filename,
            'stock_alert_qty' => $request->stock_alert_qty,
        ]);

        return redirect()->route('product.index')->with('success', 'Product has been updated successfully');
    }
}
